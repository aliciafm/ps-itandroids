// Alicia Fortes Machado / Maromba
// PS da Androids - Lista Ligada

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <cstdio>

#define MAX 100

using namespace std;

class part{
public:
    char parteCorpo[MAX];
    int nojoints = 0;//inicializa 0
    char joints[MAX][MAX];
    part * prox = NULL;//inicializa null
    void print();
};

void part::print()
{
    cout << "<" << parteCorpo << ">;" << endl;
    cout << "Numero de juntas: " << "<" << nojoints << ">" << endl;
    cout << "Nome das juntas:" << endl;
    for(int i=0; i<nojoints; i++)
    {
        cout << "<" << parteCorpo << " " << joints[i] << ">;" << endl;
    }
}

void menu()
{
    cout << "MENU" << endl << "1 - Escolher junta para consultar" << endl << "2 - Encerrar o programa" << endl;
}

int main()
{
    part * ini, * p, * q;
    FILE * entrada;
    char parteLida[MAX];
    entrada = fopen("dados.txt","r");
    //obtem os dados da file
    //primeiro pega o primeiro nó
    q = (part *)malloc(sizeof(part));
    fscanf(entrada, "%s %s", q->parteCorpo, q->joints[q->nojoints]);
    q->nojoints=q->nojoints+1;
    fscanf(entrada, "%s", parteLida);
    while(strcmp(parteLida, q->parteCorpo)==0)
    {
        fscanf(entrada, "%s", q->joints[q->nojoints]);
        q->nojoints=q->nojoints+1;
        fscanf(entrada, "%s", parteLida);
    }
    ini = q;
    while(!feof(entrada))
    {
        p = (part *)malloc(sizeof(part));
        strcpy(p->parteCorpo, parteLida);
        fscanf(entrada, "%s", p->joints[(p->nojoints)++]);
        fscanf(entrada, "%s", parteLida);
        while (strcmp(parteLida, p->parteCorpo)==0 && !feof(entrada))
        {
            fscanf(entrada, "%s", p->joints[(p->nojoints)++]);
            fscanf(entrada, "%s", parteLida);
        }
        q->prox = p;
        q = p;
    }
    //agora ordena a lista ligada - bubble sort
    bool ordenado = false;
    while(!ordenado)
    {
        part * aux;
        ordenado = true;
        p = ini;
        while(p != NULL && p->prox != NULL)
        {
            if(strcmp(p->parteCorpo, (p->prox)->parteCorpo)>0)
            {
                ordenado = false;
                if(p == ini)
                {
                    aux = p->prox;
                    p->prox = (p->prox)->prox;
                    aux->prox = p;
                    ini = aux;
                }

                else
                {
                    aux = p;
                    q->prox = p->prox;
                    p->prox = (p->prox)->prox;
                    (q->prox)->prox = aux;
                }
            }
            q = p;
            p = p->prox;
        }
    }
    //agora ordena cada um dos nós
    p=ini;
    while(p!=NULL)
    {
        bool ord = false;
        while(!ord)
        {
            char aux[MAX];
            ord = true;
            for(int i=0; i < p->nojoints - 1; i++)
            {
                if(strcmp(p->joints[i],p->joints[i+1])>0)
                {
                    ord = false;
                    strcpy(aux, p->joints[i]);
                    strcpy(p->joints[i], p->joints[i+1]);
                    strcpy(p->joints[i+1], aux);
                }
            }
        }
        p=p->prox;
    }
    // apresenta o menu
    int opcao;
    menu();
    cin >> opcao;
    system("clear");
    while(opcao!=2)
    {
        char parteProcurada[MAX];
        cout << "Qual o nome da parte do corpo que esta interessado (em letras maiusculas) >> " << flush;
        cin >> parteProcurada;
        // busca(parteProcurada);
        p = ini;
        while(p!=NULL && strcmp(parteProcurada, p->parteCorpo)!=0)
        {
            p=p->prox;
        }
        if(p==NULL)
            cout << "Parte nao encontrada" << endl;
        else
        {
            cout << "Seguem as caracteristicas" << endl;
            p->print();
        }
        cout << "Digite enter para continuar . . ." << endl;
        getchar();
        getchar();
        system("clear");
        menu();
        cin >> opcao;
        system("clear");
    }
    return 0;
}
