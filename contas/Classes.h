#ifndef CLASSES_H_INCLUDED
#define CLASSES_H_INCLUDED

#include <vector>

#define MAX 1000

class Data{
public:
    int dia;
    int mes;
    int ano;
    void imprimeData();
};

class Transacao{
public:
    float val;
    char descricao[MAX];
    Data data;
    void getTransacao();
};

class Conta{
public:
    char nomeCorrentista[MAX];
    int numeroConta;
    int qtdTrans = 0;
    float saldo=0;
    void getNome();
    void getNum();
    Transacao transacoes[MAX];
    void deposito(float valor);
};

class ContaLimitada: public Conta{
public:
    float limite;
    void extrato();
    void retirada(float valor);
    ContaLimitada();
};

class ContaComum: public Conta{
public:
    void extrato();
    void retirada(float valor);
    ContaComum();
};

#endif // CLASSES_H_INCLUDED
