#include "Classes.h"
#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

#define MAX 1000

void Data::imprimeData()
{
    cout << dia << "/" << mes << "/" << ano << flush;
}

void Transacao::getTransacao(){
    cout << descricao << " --- " << flush;
    printf("%.2f", val);
    cout << " --- " << flush;
    data.imprimeData();
    cout << endl;
}

ContaLimitada::ContaLimitada():Conta()
{
    saldo = 0;
    qtdTrans = 0;
}

ContaComum::ContaComum():Conta()
{
    saldo = 0;
    qtdTrans = 0;
}

void ContaComum::extrato()
{
    cout << "Numero da conta: " << numeroConta << endl;
    cout << "Cliente: " << nomeCorrentista << endl;
    cout << "Transacoes:" << endl;
    for(int i=0; i<qtdTrans; i++)
    {
        transacoes[i].getTransacao();
    }
    printf("Saldo: %.2f\n", saldo);
}

void ContaLimitada::extrato()
{
    cout << "Numero da conta: " << numeroConta << endl;
    cout << "Cliente: " << nomeCorrentista << endl;
    cout << "Transacoes:" << endl;
    for(int i=0; i<qtdTrans; i++)
    {
        transacoes[i].getTransacao();
    }
    printf("Saldo: %.2f\n", saldo);
    cout << "Limite da conta: " << limite << endl;
}

void Conta::deposito(float valor)
{
    saldo += valor;
}

void ContaLimitada::retirada(float valor)
{
    if(saldo-valor>-limite)
        saldo -=valor;
    else cout << "Operacao invalida devido o limite ter sido alcancado" << endl;
}

void ContaComum::retirada(float valor)
{
    saldo-=valor;
}

void Conta::getNome()
{
    cin >> nomeCorrentista;
}

void Conta::getNum()
{
    cin >> numeroConta;
}
