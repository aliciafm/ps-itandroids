#include <cstdio>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include "Classes.h"

#define MAX 1000

using namespace std;

int main()
{
    vector<ContaComum> contasC;
    vector<ContaLimitada> contasL;
    int opcao = 0;
    //adicionar conta
    cout << "MENU" << endl << "1 - Adicionar conta" << endl;
    cout << "2 - Realizar transacao" << endl << "3 - Sair" << endl;
    while(opcao != 3)
    {
        if(opcao == 1)
        {
            char r;
            cout << "Conta com limite? (s/n)" << endl;
            cin >> r;
            if(r == 's')
            {
                ContaLimitada contaNova;
                cout << "Digite o numero do concorrentista >>" << endl;
                contaNova.getNome();
                cout << "Digite o numero da conta >>" << endl;
                contaNova.getNum();
                cout << "Digite o limite >>" << endl;
                cin >> contaNova.limite;
                contasL.push_back(contaNova);
            }
            else if(r == 'n')
            {
                ContaComum contaNova;
                cout << "Digite o nome do concorrentista >>" << endl;
                contaNova.getNome();
                cout << "Digite o numero da conta >>" << endl;
                contaNova.getNum();
                contasC.push_back(contaNova);
            }
        }
        else if(opcao == 2)
        {
            int numConta;
            char tr;
            cout << "Qual o numero da conta?" << endl;
            cin >> numConta;
            //achar qual eh a conta
            int achou = 0;
            vector<ContaLimitada>::iterator it2;
            vector<ContaComum>::iterator it;
            it = contasC.begin();
            while(it != contasC.end() && achou == 0)
            {
                if(it->numeroConta == numConta)
                {
                    achou = 1;
                }
                else it++;
            }
            if(achou == 0)
            {
                it2 = contasL.begin();
                while(it2!=contasL.end() && achou == 0)
                {
                    if(it2->numeroConta == numConta)
                    {
                        achou = 2;
                    }
                    else it2++;
                }
            }
            if(achou == 0)
                cout << "Conta nao encontrada" << endl;
            else
            {
                cout << "Qual transacao deseja realizar?" << endl << "1 - Deposito (d)" << endl;
                cout << "2 - Retirada (r)" << endl << "3 - Extrato (e)" << endl;
                cin >> tr;
                if(tr == 'd')
                {
                    Transacao trans;
                    strcpy(trans.descricao, "Deposito");
                    cout << "Quanto deseja depositar?" << endl;
                    cin >> trans.val;
                    cout << "Em que data? Coloque da forma dd mm aaaa." << endl;
                    cin >> trans.data.dia >> trans.data.mes >> trans.data.ano;
                    if(achou == 1)
                    {
                        it->deposito(trans.val);
                        it->transacoes[it->qtdTrans]=trans;
                        it->qtdTrans++;
                    }
                    else
                    {
                        it2->deposito(trans.val);
                        it2->transacoes[it2->qtdTrans]=trans;
                        it2->qtdTrans++;
                    }
                }
                else if(tr == 'r')
                {
                    Transacao trans;
                    strcpy(trans.descricao, "Retirada");
                    cout << "Quanto deseja retirar?" << endl;
                    cin >> trans.val;
                    cout << "Em que data? Coloque da forma dd mm aaaa." << endl;
                    cin >> trans.data.dia >> trans.data.mes >> trans.data.ano;
                    if(achou == 1)
                    {
                        it->retirada(trans.val);
                        it->transacoes[it->qtdTrans]=trans;
                        it->qtdTrans++;
                    }
                    else
                    {
                        it2->retirada(trans.val);
                        it2->transacoes[it2->qtdTrans]=trans;
                        it2->qtdTrans++;
                    }
                }
                else if(tr == 'e')
                {
                    if(achou == 1)
                    {
                        it->extrato();
                    }
                    else
                    {
                        it2->extrato();
                    }
                    system("pause");
                }
            }
        }
        system("cls");
        cout << "MENU" << endl << "1 - Adicionar conta" << endl;
        cout << "2 - Realizar transacao" << endl << "3 - Sair" << endl;
        cin >> opcao;
        system ("cls");
    }
    //encerrar o programa
    return 0;
}
